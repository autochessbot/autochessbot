const {Command, Args, Reply} = require('../command.js');
const Tournament = require('../../schema/tournament.js');
const User = require('../../schema/user.js');
const dacService = require('../../lib/dac-service.js');
const rankUtil = require('../../lib/rank-util.js');
const steamService = require('../../lib/steam-service.js');

function createRegistration(discordNameTag, user, tregion, tournament, tournamentRole, discordUser) {
    return steamService.getSteamPersonaName(user.steam).then(steamName =>
        Tournament
            .createRegistration(discordNameTag, user.steam, steamName, tregion)
            .then(registration => registration.setUser(user))
            .then(registration => tournament.addRegistration(registration))
            .then(() => Reply.channelMention(`You have successfully registered for \`${tournament.name}\``))
    );
}

function updateRegistration(registration, discordNameTag, user, tregion, tournament) {
    return Reply.channelMention(`You have already registered for \`${tournament.name}\` in Region: \`${registration.region}\``);
}

function action(message, args) {
    return Tournament.latest().then(tournament =>
        User.findByDiscord(message.author.id).then(user => dacService.getRankFromSteamId(user.steam).then(rank => {
            let requirementUnmet = (rank.mmr_level < tournament.minRank);

            //Queen Tournament
            let queenMinRank = rankUtil.getQueenRank(tournament.minRank);
            if (queenMinRank != null) {
                let queenRank = rankUtil.getQueenRank(rank.mmr_level);
                requirementUnmet = (queenRank === null || queenRank > queenMinRank);
            }

            if (requirementUnmet) {
                return Reply.channelMention(`Tournament rank requirement: ${rankUtil.getRankString(tournament.minRank)}. Your rank: ${rankUtil.getRankString(rank.mmr_level)}`);
            } else {
                let discordNameTag = `${message.author.username}#${message.author.discriminator}`;
                let tournamentRole = message.guild.roles.find(r => r.name === 'Tournament'); // todo: use config
                let tregion = args.tournamentregion.toUpperCase();
                return Tournament.findRegistration(tournament.id, user.id).then(registration =>
                    registration === null ?
                        createRegistration(discordNameTag, user, tregion, tournament, tournamentRole, message.member)
                        : updateRegistration(registration, discordNameTag, user, tregion, tournament));
            }
        })));
}

module.exports = new Command({
    name: 'register',
    aliases: ['checkin'],
    args: [
        Args.TOURNAMENTREGION
    ],
    allowedChannels: ['tournament-signup'],
    action: action
});