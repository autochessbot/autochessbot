const {Command} = require('../command.js');

module.exports = new Command({
    name: 'r',
    aliases: ['rank', 'updateroles'],
    dmOnly: true,
    action: function (message, args, injections) {
        injections.rankBatch.addRankCheck(message.author.id);
        return Promise.resolve({});
    }
});