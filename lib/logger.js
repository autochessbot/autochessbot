const winston = require("winston");
const config = require("../config");

const format = winston.format;
const logger = winston.createLogger({
    level: 'error',
    format: format.combine(format.timestamp({format: 'YYYY-MM-DD HH:mm:ss'}), format.json()),
    transports: [
        new winston.transports.File({filename: config.logfile_error, level: 'error'}),
        new winston.transports.File({filename: config.logfile})
    ]
});

module.exports = logger;
