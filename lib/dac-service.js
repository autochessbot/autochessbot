const logger = require('./logger.js');
const rp = require('request-promise');
const metrics = require("./metrics");

module.exports = {

    getRankFromSteamId: function (steamId) {
        return this.getRankFromSteamIds([steamId])
            .then(ranks => ranks !== null && ranks.hasOwnProperty(steamId) ? ranks[steamId] : null);
    },

    getRankFromSteamIds: function (steamIds) {
        const end = metrics.dacRequestHistogram.startTimer();

        return rp({
            uri: "http://autochess.ppbizon.com/ranking/get",
            method: "GET",
            json: true,
            qs: {
                player_ids: steamIds.join(',')
            },
            headers: {'User-Agent': 'Valve/Steam HTTP Client 1.0 (570;Windows;tenfoot)'},
            timeout: 5000
        }).then(res => {
            end();

            const ranks = res.ranking_info.reduce((map, rankingInfo) => {
                let queenRank = parseInt(rankingInfo.queen_rank);
                let parsedRank = parseInt(rankingInfo.mmr_level);
                if (parsedRank >= 38) { // because sometimes we get queen_rank field even when the user is not queen
                    parsedRank = parsedRank + (queenRank ? queenRank : 0);
                }

                map[rankingInfo.player] = {
                    matches: rankingInfo.match,
                    mmr_level: parsedRank
                };
                return map;
            }, {});

            metrics.dacRequestSuccessCounter.inc();
            return ranks;
        }).catch(err => {
            end();
            metrics.dacRequestErrorCounter.inc();
            logger.error(err.message + " " + err.stack);
            // throw err;
            // Return null for now, once all commands migrated, just throw
            return null;
        });
    }
};