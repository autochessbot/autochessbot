const metrics = require('./metrics.js');
const logger = require('./logger.js');
const rp = require('request-promise');
const config = require('../config.js');

module.exports = {
    getSteamPersonaName: function (steamId) {
        return this.getSteamPersonaNames([steamId]).then(names => names[steamId]);
    },

    getSteamPersonaNames: function (steamIds) {
        const end = metrics.steamRequestHistogram.startTimer();

        return rp({
            uri: "https://api.steampowered.com/ISteamUser/GetPlayerSummaries/v2/",
            method: "GET",
            json: true,
            qs: {
                key: config.steam_token,
                steamids: steamIds.join(',')
            },
            timeout: 5000
        }).then(res => {
            end();
            const names = res.response.players.reduce((map, player) => {
                map[player.steamid] = player.personaname;
                return map;
            }, {});
            metrics.steamRequestSuccessCounter.inc();
            return names;
        }).catch(err => {
            end();
            metrics.steamRequestErrorCounter.inc();
            logger.error(err.message + " " + err.stack);
            throw err;
        });
    }
};
