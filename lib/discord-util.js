"use strict";

const logger = require('./logger.js');
const mc = require('../process/message-consolidator.js');

const metrics = require("./metrics");

module.exports = class DiscordUtil {
    constructor(discordClient) {
        this.discordClient = discordClient;
    }

    sendChannelAndMention(channelDiscordId, userDiscordId, text) {
        let channel = this.discordClient.channels.get(channelDiscordId);
        mc.enqueueMessage(channel, text, userDiscordId);
        if (channel.type === "dm") {
            metrics.dmCounter.inc({type: 'SentViaSendChannel'});
        } else {
            metrics.sendChannelCounter.inc({'channel_name': channel.name, 'channel_id': channelDiscordId});
        }
    }

    sendChannel(channelDiscordId, text) {
        let channel = this.discordClient.channels.get(channelDiscordId);
        mc.enqueueMessage(channel, text);
        if (channel.type === "dm") {
            metrics.dmCounter.inc({type: 'SentViaSendChannel'});
        } else {
            metrics.sendChannelCounter.inc({'channel_name': channel.name, 'channel_id': channelDiscordId});
        }
    }

    // todo ratelimiting send dm
    sendDM(userDiscordId, text) {
        this.discordClient.fetchUser(userDiscordId).then(user => {
            user.send(text).catch(err => {
                if (err.code === 50007) {
                    // TODO: figure out how to send this in the channel the user sent it from... we don't have message.channel.id
                    this.sendChannelAndMention(this.discordClient.channels.find(r => r.name === "chessbot-warnings").id, userDiscordId, "I could not send a direct message to this user. They might have turned direct messages from server members off in their Discord Settings under 'Privacy & Safety'.");
                } else {
                    logger.error(err);
                }
            });
        });
        metrics.dmCounter.inc({type: 'Sent'});
    }

    // todo ratelimiting deletes
    deleteMessage(message) {
        if (message.channel.type !== "dm") {
            message.delete().catch(err => logger.error(err));
            metrics.deleteMessageCounter.inc({'channel_name': message.channel.name, 'channel_id': message.channel.id});
        }
    }

    handle(message, result) {
        switch (result.type) {
            case 'channel':
                this.sendChannel(message.channel.id, result.reply);
                break;
            case 'channelMention':
                this.sendChannelAndMention(message.channel.id, message.author.id, result.reply);
                break;
            case 'dm':
                this.sendDM(message.author.id, result.reply);
                break;
        }
        if (result.delete === true) {
            this.deleteMessage(message);
        }
    }

};