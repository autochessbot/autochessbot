const dacService = require('../lib/dac-service.js');
const config = require("../config");
const CronJob = require('cron').CronJob;
const rankUtil = require('../lib/rank-util.js');
const User = require('../schema/user.js');
const logger = require('../lib/logger.js');

class rankBatch {

    constructor() {
        this.queue = [];
        this.rankBatchCron = new CronJob(
            config.rank_batch_cron,
            () => this.batchCheckRanks(),
            null,
            /* don't start right after init */ false, 'America/Los_Angeles'
        );
    }

    inject(injections) {
        this.injections = injections;
    }

    // Fire and forget
    addRankCheck(user) {
        this.queue.push(user);
    }

    // TODO returns a promise for caller to chain
    // getRank(user) {
    //
    // }

    batchCheckRanks() {
        let guild = this.injections.guild;
        let discordUtil = this.injections.discordUtil;
        if (guild === null || !guild.available || guild.client === null) {
            logger.error('guild not ready');
            return;
        }

        let batch = this.queue;
        batch = [...new Set(batch)];
        this.queue = [];
        if (batch.length === 0) {
            return;
        }

        User.findAllUsersWithDiscordIdIn(batch).then(users => {
            return dacService
                .getRankFromSteamIds(users.map(user => user.steam))
                .then(ranks => {
                        if (ranks === null) return; // todo cleanup after dac service throws instead of return null
                        for (let user of users) {
                            if (!ranks.hasOwnProperty(user.steam)) {
                                // unranked, need to play 5 games
                                discordUtil.sendDM(user.discord, 'You need to play at least 5 games to get a rank.');
                            } else {
                                let newRank = ranks[user.steam].mmr_level;
                                let rankString = `Your current rank is ${rankUtil.getRankString(newRank)}.`;
                                if (parseInt(user.rank) === newRank && false) { // disable for now. also check for rankUpdatedAt
                                    // no rank changes
                                    discordUtil.sendDM(user.discord, `${rankString} Your rank did not change; no role updates at this time.`);
                                } else {
                                    guild.client.fetchUser(user.discord)
                                        .then(discordUser => guild.fetchMember(discordUser))
                                        .then(member => {
                                            let roleChangeString = this.updateRoles(member, newRank);
                                            discordUtil.sendDM(user.discord, `${rankString} ${roleChangeString}`);
                                        });
                                }
                                user.update({
                                    rank: newRank,
                                    // rankUpdatedAt: Date.now()
                                });
                            }
                        }
                    }
                );
        }).catch(err => {
            console.log(err);
            logger.error(err);
        });
    }

    updateRoles(member, newRank) {
        let cachedRoles = this.injections.cachedRoles;
        let newRoles = this.rolesForRank(cachedRoles, newRank);
        let currentRoles = member.roles.filter(role => cachedRoles.find(cached => cached.role === role));
        let rolesToAdd = newRoles.filter(role => !currentRoles.has(role.id));
        let rolesToRemove = currentRoles.filter(newRole => !newRoles.find(role => role.id === newRole.id)).array();
        if (rolesToRemove.length > 0) {
            member.removeRoles(rolesToRemove);
            let roleString = rolesToRemove.map(role => `\`${role.name}\``).join(',');
            return `You have been demoted from ${roleString}.`;
        } else if (rolesToAdd.length > 0) {
            member.addRoles(rolesToAdd);
            let roleString = rolesToAdd.map(role => `\`${role.name}\``).join(',');
            return `You have been promoted to ${roleString}.`;
        } else {
            return 'No role changes at this time.';
        }
    }

    startBatchCron() {
        this.rankBatchCron.start();
    }

    rolesForRank(cachedRoles, rank) {
        let queenCachedRoles = cachedRoles.filter(role => this.injections.queenRoles.includes(role.name));
        let nonQueenCachedRoles = cachedRoles.filter(role => !this.injections.queenRoles.includes(role.name));
        
        let newRoles = nonQueenCachedRoles.filter(role => role.rank <= rank).map(role => role.role);

        // Handle Queen Ranks
        let queenRank = rankUtil.getQueenRank(rank);
        if (queenRank !== null) {
            let queenRoles = queenCachedRoles.filter(role => role.rank >= queenRank).map(role => role.role);
            newRoles = newRoles.concat(queenRoles);
        }

        return newRoles;
    }
}

module.exports = new rankBatch();