const Tournament = require('./models.js').Tournament;
const Registration = require('./models.js').Registration;

const tournamentUtil = {
    createTournament: function (name, rank) {
        return Tournament.create({
            name: name,
            minRank: rank
        });
    },

    latest: function () {
        return Tournament.findOne({
            order: [['createdAt', 'DESC']]
        });
    },

    createRegistration: function (discordNameTag, steam, steamName, tregion) {
        return Registration.create({
            discord: discordNameTag,
            steam: steam,
            steamName: steamName,
            region: tregion
        });
    },

    findRegistration: function (tournamentId, userId) {
        return Registration.findOne({
            where: {
                tournamentId: tournamentId,
                userId: userId
            }
        });
    },

    findParticipantsByRegion: function (tournamentId, tregion) {
        return Registration.findAll({
            where: {
                tournamentId: tournamentId,
                region: tregion
            }
        });
    }
};

module.exports = tournamentUtil;
